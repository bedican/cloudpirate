var fs = require('fs');
var sinon = require('sinon');
var expect = require('chai').expect;
var assert = require('chai').assert;
var config = require('../../lib/aws/config');

describe('Aws config', function() {
    describe('#getConfig', function() {

        var mockFs;
        var mockConfig = JSON.stringify({
            aws: {
                test: 'test'
            }
        });

        config.getCwd = function() {
            return '/my/cwd';
        };

        beforeEach(function() {
            mockFs = sinon.mock(fs);
        });

        afterEach(function() {
            mockFs.restore();
        });

        it('should use default filename', function() {

            mockFs.expects('readFileSync').once().withArgs('/my/cwd/cloudpirate-aws.json').returns(mockConfig);

            var awsConfig = config.getConfig({
                awsConfig: false
            });
        });

        it('should use filename set via configuration', function() {

            mockFs.expects('readFileSync').once().withArgs('/path/to/custom.json').returns(mockConfig);

            var awsConfig = config.getConfig({
                awsConfig: '/path/to/custom.json'
            });
        });

        it('should throw error on read failure', function() {
            mockFs.expects('readFileSync').once().throws();
            assert.throws(function() { config.getConfig({}); }, Error, 'Failed to read aws configuration');
        });
    });
});
