var sinon = require('sinon');
var expect = require('chai').expect;
var factory = require('../../lib/aws/aws-service-factory');
var config = require('../../lib/aws/config');
var ec2 = require('../../lib/aws/ec2');

describe('Aws ec2', function() {
    describe('#servers', function() {

        var mockFactory;
        var mockConfig;

        var ec2service = {
            describeInstances: function(params, callback) {
                callback(false, {
                    Reservations: [
                        {
                            Instances: [
                                {
                                    Tags: [
                                        { Key: 'cloudpirate.env', Value: 'env' },
                                        { Key: 'cloudpirate.app', Value: 'app' }
                                    ],
                                    InstanceId: 'id1',
                                    PublicDnsName: 'pubdns',
                                    PublicIpAddress: 'pubip',
                                    PrivateIpAddress: 'privip',
                                    PrivateDnsName: 'privdns',
                                    InstanceType: 'type',
                                    LaunchTime: '123'
                                },
                                {
                                    Tags: [
                                        { Key: 'cloudpirate.env', Value: 'env' },
                                        { Key: 'cloudpirate.app', Value: 'app' },
                                        { Key: 'cloudpirate.role', Value: 'role' }
                                    ],
                                    InstanceId: 'id2',
                                    PublicDnsName: 'pubdns',
                                    PublicIpAddress: 'pubip',
                                    PrivateIpAddress: 'privip',
                                    PrivateDnsName: 'privdns',
                                    InstanceType: 'type',
                                    LaunchTime: '123'
                                },
                                {
                                    Tags: [
                                        { Key: 'cloudpirate.env', Value: 'env' },
                                        { Key: 'cloudpirate.app', Value: 'app2' }
                                    ],
                                    InstanceId: 'id3',
                                    PublicDnsName: 'pubdns',
                                    PublicIpAddress: 'pubip',
                                    PrivateIpAddress: 'privip',
                                    PrivateDnsName: 'privdns',
                                    InstanceType: 'type',
                                    LaunchTime: '123'
                                },
                                {
                                    Tags: [
                                        { Key: 'cloudpirate.env', Value: 'env2' },
                                        { Key: 'cloudpirate.app', Value: 'app' }
                                    ],
                                    InstanceId: 'id4',
                                    PublicDnsName: 'pubdns',
                                    PublicIpAddress: 'pubip',
                                    PrivateIpAddress: 'privip',
                                    PrivateDnsName: 'privdns',
                                    InstanceType: 'type',
                                    LaunchTime: '123'
                                }
                            ]
                        }
                    ]
                });
            }
        };

        beforeEach(function() {
            mockConfig = sinon.mock(config);
            mockConfig.expects('getConfig').once().returns({});

            mockFactory = sinon.mock(factory);
            mockFactory.expects('ec2').once().returns(ec2service);
        });

        afterEach(function() {
            mockFactory.restore();
            mockConfig.restore();
        });

        it('should return all instances with the given app and env', function(done) {
            ec2.servers('env', 'app', false, {}, function(err, servers) {
                expect(err).to.be.false;
                expect(servers).to.have.length(2);

                expect(servers[0].id).to.eql('id1');
                expect(servers[1].id).to.eql('id2');

                done();
            });
        });

        it('should return all instances with the given app and env and role', function(done) {
            ec2.servers('env', 'app', 'role', {}, function(err, servers) {
                expect(err).to.be.false;
                expect(servers).to.have.length(1);

                expect(servers[0].id).to.eql('id2');

                done();
            });
        });

        it('should return all instances with the given app2 and env', function(done) {
            ec2.servers('env', 'app2', false, {}, function(err, servers) {
                expect(err).to.be.false;
                expect(servers).to.have.length(1);

                expect(servers[0].id).to.eql('id3');

                done();
            });
        });

        it('should return all instances with the given app and env2', function(done) {
            ec2.servers('env2', 'app', false, {}, function(err, servers) {
                expect(err).to.be.false;
                expect(servers).to.have.length(1);

                expect(servers[0].id).to.eql('id4');

                done();
            });
        });
    });
});
