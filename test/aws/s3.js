var fs = require('fs');
var sinon = require('sinon');
var expect = require('chai').expect;
var factory = require('../../lib/aws/aws-service-factory');
var config = require('../../lib/aws/config');
var s3 = require('../../lib/aws/s3');

describe('Aws s3', function() {

    var mockFactory, mockFs, mockConfig;

    beforeEach(function() {
        mockFactory = sinon.mock(factory);
        mockFs = sinon.mock(fs);

        mockConfig = sinon.mock(config);
        mockConfig.expects('getConfig').once().returns({
            aws: {
            },
            s3: {
                bucket: 'mock-bucket'
            }
        });
    });

    afterEach(function() {
        mockFactory.restore();
        mockFs.restore();
        mockConfig.restore();
    });

    function mockS3service(onEvent, responseData) {

        var response = {
            data: {
                Body: responseData || ''
            }
        };

        var future = {
            callback: false,
            invokeCallback: function() {
                this.callback(response);
            }
        };

        var request = {
            on: function(event, callback) {
                if (event == onEvent) {
                    future.callback = callback;
                }

                return this;
            },
            send: function() {
                return this;
            },
            createReadStream: function() {
                return {
                    pipe: function(stream) {}
                };
            }
        };

        var s3service = {
            putObject: function(params) {
                return request;
            },
            getObject: function(params) {
                return request;
            }
        };

        mockFactory.expects('s3').once().returns(s3service);

        return future;
    }

    describe('#upload', function() {

        beforeEach(function() {
            mockFs.expects('createReadStream').once().returns('read stream');
        });

        it('should callback with result of true on success', function(done) {

            var future = mockS3service('success');

            s3.upload('filename', 'key', {}, function(success) {
                expect(success).to.be.true;
                done();
            });

            future.invokeCallback();
        });

        it('should callback with result of false on error', function(done) {

            var future = mockS3service('error');

            s3.upload('filename', 'key', {}, function(success) {
                expect(success).to.be.false;
                done();
            });

            future.invokeCallback();
        });

        it('should use app config bucket value over aws config value', function(done) {

            var futureParams;

            var callback = function(success) {
                expect(futureParams.Bucket).to.eql('mock-bucket-2');
                done();
            };

            var mockRequest = {
                on: function() {
                    return this;
                },
                send: function() {
                    callback();
                    return this;
                }
            };

            var mockService = {
                putObject: function(params) {
                    futureParams = params;
                    return mockRequest;
                }
            };

            mockFactory.expects('s3').once().returns(mockService);

            s3.upload('filename', 'key', { bucket: 'mock-bucket-2' }, callback);
        });
    });

    describe('#download', function() {

        beforeEach(function() {
            mockFs.expects('createWriteStream').once().returns('write stream');
        });

        it('should callback with result of true on success', function(done) {

            var future = mockS3service('success');

            s3.download('key', 'filename', {}, function(success) {
                expect(success).to.be.true;
                done();
            });

            future.invokeCallback();
        });

        it('should callback with result of false on error', function(done) {

            var future = mockS3service('error');

            s3.download('key', 'filename', {}, function(success) {
                expect(success).to.be.false;
                done();
            });

            future.invokeCallback();
        });

        it('should use app config bucket value over aws config value', function(done) {

            var futureParams;

            var callback = function(success) {
                expect(futureParams.Bucket).to.eql('mock-bucket-2');
                done();
            };

            var mockRequest = {
                on: function() {
                    return this;
                },
                createReadStream: function() {
                    return this;
                },
                pipe: function() {
                    callback();
                    return this;
                }
            };

            var mockService = {
                getObject: function(params) {
                    futureParams = params;
                    return mockRequest;
                }
            };

            mockFactory.expects('s3').once().returns(mockService);

            s3.download('filename', 'key', { bucket: 'mock-bucket-2' }, callback);
        });
    });

    describe('#content', function() {

        it('should callback with data on success', function(done) {

            var future = mockS3service('success', 'response data');

            s3.content('key', {}, function(data) {
                expect(data).to.eql('response data');
                done();
            });

            future.invokeCallback();
        });

        it('should callback with result of false on error', function(done) {

            var future = mockS3service('error');

            s3.content('key', {}, function(data) {
                expect(data).to.be.false;
                done();
            });

            future.invokeCallback();
        });

        it('should use app config bucket value over aws config value', function(done) {

            var futureParams;

            var callback = function(success) {
                expect(futureParams.Bucket).to.eql('mock-bucket-2');
                done();
            };

            var mockRequest = {
                on: function() {
                    return this;
                },
                send: function() {
                    callback();
                    return this;
                }
            };

            var mockService = {
                getObject: function(params) {
                    futureParams = params;
                    return mockRequest;
                }
            };

            mockFactory.expects('s3').once().returns(mockService);

            s3.content('key', { bucket: 'mock-bucket-2' }, callback);
        });
    });
});
