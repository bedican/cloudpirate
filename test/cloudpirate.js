var sinon = require('sinon');
var expect = require('chai').expect;
var assert = require('chai').assert;

var CloudPirate = require('../lib/cloudpirate');
var Config = require('../lib/config');

var task = require('../lib/tasks/task');
var aws = require('../lib/aws/aws');

describe('Cloud Pirate', function() {
    describe('Configure flightplan', function() {

        var mockTask, mockConfig;
        var config = new Config({});

        var app1Config = {
            username: 'username',
            sshKey: 'sshKey',
            envs: [ 'env1', 'env2' ],
            roles: {
                role1: {
                    task1: 'task1'
                }
            }
        };

        var app2Config = {
            username: 'username',
            sshKey: 'sshKey',
            envs: [ 'env3', 'env4' ],
            roles: {
                role2: {
                    task2: 'task2',
                    task3: 'task3'
                }
            }
        };

        beforeEach(function() {
            mockConfig = sinon.mock(config);

            mockConfig.expects('getApps').returns(['app1', 'app2']);
            mockConfig.expects('getEnvs').withArgs('app1').returns(['env1', 'env2']);
            mockConfig.expects('getEnvs').withArgs('app2').returns(['env3', 'env4']);
            mockConfig.expects('getAppConfig').withArgs('app1', 'env1').returns(app1Config);
            mockConfig.expects('getAppConfig').withArgs('app1', 'env2').returns(app1Config);
            mockConfig.expects('getAppConfig').withArgs('app2', 'env3').returns(app2Config);
            mockConfig.expects('getAppConfig').withArgs('app2', 'env4').returns(app2Config);

            mockTask = {};
            for (var t in task) {
                mockTask[t] = sinon.mock(task[t]);
            }
        });

        afterEach(function() {
            mockConfig.restore();

            for (var t in mockTask) {
                mockTask[t].restore();
            }
        });

        it('should create tasks', function() {

            var plan = {
                target: function() {}
            };

            mockTask.deploy.expects('create').once();
            mockTask.build.expects('create').once();
            mockTask.instances.expects('create').once();
            mockTask.version.expects('create').once();
            mockTask.roletask.expects('create').thrice();

            var cloudpirate = new CloudPirate(plan);
            cloudpirate.configure(config);

            for (var t in mockTask) {
                mockTask[t].verify();
            }
        });

        it('should create targets', function() {

            for (var t in mockTask) {
                mockTask[t].expects('create').atLeast(1);
            }

            var targets = [];
            var plan = {
                target: function(name, hostsOrFn) {
                    targets.push(name);
                }
            };

            var cloudpirate = new CloudPirate(plan);
            cloudpirate.configure(config);

            expect(targets.sort()).to.eql([
                'app1',
                'app2',
                'app1.env1',
                'app1.env2',
                'app2.env3',
                'app2.env4',
                'app1.env1.role1',
                'app1.env2.role1',
                'app2.env3.role2',
                'app2.env4.role2'
            ].sort());
        });

        it('should throw error if username is not defined', function() {
            var plan = {
                target: function() {}
            };

            var cloudpirate = new CloudPirate(plan);

            assert.throws(function() { cloudpirate.createTarget('env', 'app', false, { sshKey: 'sshKey' }); }, Error, 'Missing username or sshKey configuration options.');
        });

        it('should throw error if sshKey is not defined', function() {
            var plan = {
                target: function() {}
            };

            var cloudpirate = new CloudPirate(plan);

            assert.throws(function() { cloudpirate.createTarget('env', 'app', false, { username: 'username' }); }, Error, 'Missing username or sshKey configuration options.');
        });

        it('should return configured hosts', function() {

            var plan = {
                target: function() {}
            };

            var cloudpirate = new CloudPirate(plan);
            var hosts = cloudpirate.createTarget('env', 'app', false, {
                username: 'username',
                sshKey: 'sshKey',
                servers: {
                    default: [ 'host1', 'host2', 'host3' ],
                    role1: [ 'host1', 'host2' ],
                    role2: [ 'host3' ]
                }
            });

            expect(hosts).to.have.length(3);

            expect(hosts[0].host).to.eql('host1');
            expect(hosts[1].host).to.eql('host2');
            expect(hosts[2].host).to.eql('host3');

            expect(hosts[0].username).to.eql('username');
            expect(hosts[1].username).to.eql('username');
            expect(hosts[2].username).to.eql('username');

            expect(hosts[0].privateKey).to.eql('sshKey');
            expect(hosts[1].privateKey).to.eql('sshKey');
            expect(hosts[2].privateKey).to.eql('sshKey');
        });

        it('should return configured hosts for role', function() {

            var plan = {
                target: function() {}
            };

            var cloudpirate = new CloudPirate(plan);
            var hosts = cloudpirate.createTarget('env', 'app', 'role1', {
                username: 'username',
                sshKey: 'sshKey',
                servers: {
                    default: [ 'host1', 'host2', 'host3' ],
                    role1: [ 'host1', 'host2' ],
                    role2: [ 'host3' ]
                }
            });

            expect(hosts).to.have.length(2);

            expect(hosts[0].host).to.eql('host1');
            expect(hosts[1].host).to.eql('host2');

            expect(hosts[0].username).to.eql('username');
            expect(hosts[1].username).to.eql('username');

            expect(hosts[0].privateKey).to.eql('sshKey');
            expect(hosts[1].privateKey).to.eql('sshKey');
        });

        it('should return first host from default for "single" role', function() {

            var plan = {
                target: function() {}
            };

            var cloudpirate = new CloudPirate(plan);
            var hosts = cloudpirate.createTarget('env', 'app', 'single', {
                username: 'username',
                sshKey: 'sshKey',
                servers: {
                    default: [ 'host1', 'host2', 'host3' ],
                    role1: [ 'host2', 'host3' ],
                    role2: [ 'host3' ]
                }
            });

            expect(hosts).to.have.length(1);

            expect(hosts[0].host).to.eql('host1');
            expect(hosts[0].username).to.eql('username');
            expect(hosts[0].privateKey).to.eql('sshKey');
        });

        it('should return configured hosts for "single" role', function() {

            var plan = {
                target: function() {}
            };

            var cloudpirate = new CloudPirate(plan);
            var hosts = cloudpirate.createTarget('env', 'app', 'single', {
                username: 'username',
                sshKey: 'sshKey',
                servers: {
                    default: [ 'host1', 'host2', 'host3' ],
                    single: [ 'host2', 'host3' ]
                }
            });

            expect(hosts).to.have.length(2);

            expect(hosts[0].host).to.eql('host2');
            expect(hosts[1].host).to.eql('host3');

            expect(hosts[0].username).to.eql('username');
            expect(hosts[1].username).to.eql('username');

            expect(hosts[0].privateKey).to.eql('sshKey');
            expect(hosts[1].privateKey).to.eql('sshKey');
        });

        it('should dynamically create hosts', function(done) {

            var plan = {
                target: function() {}
            };

            aws.ec2 = {
                servers: function(env, app, role, appConfig, callback) {
                    callback(false, [
                        {
                            id: 'id1',
                            host: 'host1',
                            ip: 'ip1',
                            ipPrivate: 'ipPrivate1',
                            dns: 'dns1',
                            dnsPrivate: 'dnsPrivate1',
                            type: 'type1',
                            time: 'time1',
                            tags : {}
                        },
                        {
                            id: 'id2',
                            host: 'host2',
                            ip: 'ip2',
                            ipPrivate: 'ipPrivate2',
                            dns: 'dns2',
                            dnsPrivate: 'dnsPrivate2',
                            type: 'type2',
                            time: 'time2',
                            tags : {}
                        }
                    ]);
                }
            };

            var cloudpirate = new CloudPirate(plan);
            var callback = cloudpirate.createTarget('env', 'app', 'role', {
                username: 'username',
                sshKey: 'sshKey'
            });

            callback(function(hosts) {
                expect(hosts).to.have.length(2);

                expect(hosts[0].host).to.eql('host1');
                expect(hosts[1].host).to.eql('host2');

                expect(hosts[0].username).to.eql('username');
                expect(hosts[1].username).to.eql('username');

                expect(hosts[0].privateKey).to.eql('sshKey');
                expect(hosts[1].privateKey).to.eql('sshKey');

                done();
            });
        });

        it('should dynamically create hosts using first host for "single" role', function(done) {

            var plan = {
                target: function() {}
            };

            var calls = 0;

            aws.ec2 = {
                servers: function(env, app, role, appConfig, callback) {

                    calls++;

                    if (calls == 1) {
                        callback(false, []);
                        return;
                    }

                    callback(false, [
                        {
                            id: 'id1',
                            host: 'host1',
                            ip: 'ip1',
                            ipPrivate: 'ipPrivate1',
                            dns: 'dns1',
                            dnsPrivate: 'dnsPrivate1',
                            type: 'type1',
                            time: 'time1',
                            tags : {}
                        },
                        {
                            id: 'id2',
                            host: 'host2',
                            ip: 'ip2',
                            ipPrivate: 'ipPrivate2',
                            dns: 'dns2',
                            dnsPrivate: 'dnsPrivate2',
                            type: 'type2',
                            time: 'time2',
                            tags : {}
                        }
                    ]);
                }
            };

            var cloudpirate = new CloudPirate(plan);
            var callback = cloudpirate.createTarget('env', 'app', 'single', {
                username: 'username',
                sshKey: 'sshKey'
            });

            callback(function(hosts) {

                expect(calls).to.eql(2);

                expect(hosts).to.have.length(1);

                expect(hosts[0].host).to.eql('host1');
                expect(hosts[0].username).to.eql('username');
                expect(hosts[0].privateKey).to.eql('sshKey');

                done();
            });
        });

        it('should dynamically create hosts using a "single" role', function(done) {

            var plan = {
                target: function() {}
            };

            aws.ec2 = {
                servers: function(env, app, role, appConfig, callback) {
                    callback(false, [
                        {
                            id: 'id1',
                            host: 'host1',
                            ip: 'ip1',
                            ipPrivate: 'ipPrivate1',
                            dns: 'dns1',
                            dnsPrivate: 'dnsPrivate1',
                            type: 'type1',
                            time: 'time1',
                            tags : {}
                        },
                        {
                            id: 'id2',
                            host: 'host2',
                            ip: 'ip2',
                            ipPrivate: 'ipPrivate2',
                            dns: 'dns2',
                            dnsPrivate: 'dnsPrivate2',
                            type: 'type2',
                            time: 'time2',
                            tags : {}
                        }
                    ]);
                }
            };

            var cloudpirate = new CloudPirate(plan);
            var callback = cloudpirate.createTarget('env', 'app', 'single', {
                username: 'username',
                sshKey: 'sshKey'
            });

            callback(function(hosts) {
                expect(hosts).to.have.length(2);

                expect(hosts[0].host).to.eql('host1');
                expect(hosts[1].host).to.eql('host2');

                expect(hosts[0].username).to.eql('username');
                expect(hosts[1].username).to.eql('username');

                expect(hosts[0].privateKey).to.eql('sshKey');
                expect(hosts[1].privateKey).to.eql('sshKey');

                done();
            });
        });
    });
});
