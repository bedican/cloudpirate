var sinon = require('sinon');
var expect = require('chai').expect;
var assert = require('chai').assert;
var version = require('../../lib/tasks/version');
var factory = require('../../lib/context-factory');
var Config = require('../../lib/config');

describe('Version task', function() {

    var mockFactory, mockConfig;
    var config = new Config({});

    beforeEach(function() {
        mockFactory = sinon.mock(factory);
        mockConfig = sinon.mock(config);
    });

    afterEach(function() {
        mockFactory.restore();
        mockConfig.restore();
    });

    describe('validation', function() {

        var plan = {
            remote: function() {},
            local: function(task, callback) {
                callback();
            },
            abort: function(msg) {
                throw new Error(msg);
            }
        };

        it('should abort if context containes role', function() {

            mockFactory.expects('create').returns({
                app: 'app',
                env: 'env',
                role: 'role',
                task: 'task',
                target: 'app.env.role',
                args: []
            });

            assert.throws(function() { version.create('task', plan, config); }, Error, 'task task must be run across all roles in an environment');
        });
    });
});
