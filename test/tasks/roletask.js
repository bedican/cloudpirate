var sinon = require('sinon');
var expect = require('chai').expect;
var assert = require('chai').assert;
var roletask = require('../../lib/tasks/roletask');
var factory = require('../../lib/context-factory');
var Config = require('../../lib/config');

describe('Role task', function() {

    var mockFactory, mockConfig;
    var config = new Config({});

    beforeEach(function() {
        mockFactory = sinon.mock(factory);
        mockConfig = sinon.mock(config);
    });

    afterEach(function() {
        mockFactory.restore();
        mockConfig.restore();
    });

    describe('validation', function() {

        var plan = {
            remote: function() {},
            local: function(task, callback) {
                callback();
            },
            abort: function(msg) {
                throw new Error(msg);
            }
        };

        it('should abort if no role in context', function() {

            mockConfig.expects('getAppConfig').withArgs('app', 'env').returns({});

            mockFactory.expects('create').returns({
                app: 'app',
                env: 'env',
                role: false,
                task: 'task',
                target: 'app.env',
                args: []
            });

            assert.throws(function() { roletask.create('task', plan, config); }, Error, 'task can not be run, no role in context');
        });

        it('should abort if no roles in app config', function() {

            mockConfig.expects('getAppConfig').withArgs('app', 'env').returns({});

            mockFactory.expects('create').returns({
                app: 'app',
                env: 'env',
                role: 'role',
                task: 'task',
                target: 'app.env.role',
                args: []
            });

            assert.throws(function() { roletask.create('task', plan, config); }, Error, 'task can not be run, no roles defined for app');
        });

        it('should abort if role in context does not exist in app config', function() {

            mockConfig.expects('getAppConfig').withArgs('app', 'env').returns({ roles: { role1: {} } });

            mockFactory.expects('create').returns({
                app: 'app',
                env: 'env',
                role: 'role',
                task: 'task',
                target: 'app.env.role',
                args: []
            });

            assert.throws(function() { roletask.create('task', plan, config); }, Error, 'task can not be run, unknown role role');
        });

        it('should abort if task does not exist for the role in app config', function() {

            mockConfig.expects('getAppConfig').withArgs('app', 'env').returns({ roles: { role: { task: '' } } });

            mockFactory.expects('create').returns({
                app: 'app',
                env: 'env',
                role: 'role',
                task: 'task',
                target: 'app.env.role',
                args: []
            });

            assert.throws(function() { roletask.create('task', plan, config); }, Error, 'task task does not exist for role role');
        });
    });

    describe('execution', function() {

        var mockTransport;

        var transport = {
            with: function(command, callback) {
                callback();
            },
            exec: function() {}
        };

        var plan = {
            local: function() {},
            remote: function(task, callback) {
                callback(transport);
            }
        };

        beforeEach(function() {
            mockTransport = sinon.mock(transport);
        });

        afterEach(function() {
            mockTransport.restore();
        });

        it('should execute command', function() {

            mockConfig.expects('getAppConfig').withArgs('app', 'env').returns({ roles: { role: { task: 'HELLO' } } });

            mockFactory.expects('create').returns({
                app: 'app',
                env: 'env',
                role: 'role',
                task: 'task',
                target: 'app.env.role',
                args: []
            });

            mockTransport.expects('exec').withArgs('HELLO');

            roletask.create('task', plan, config);

            mockTransport.verify();
        });
    });
});
