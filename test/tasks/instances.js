var sinon = require('sinon');
var expect = require('chai').expect;
var assert = require('chai').assert;
var instances = require('../../lib/tasks/instances');
var factory = require('../../lib/context-factory');
var Config = require('../../lib/config');

describe('Instance task', function() {

    var mockFactory, mockConfig;
    var config = new Config({});

    beforeEach(function() {
        mockFactory = sinon.mock(factory);
        mockConfig = sinon.mock(config);
    });

    afterEach(function() {
        mockFactory.restore();
        mockConfig.restore();
    });

    describe('validation', function() {

        var plan = {
            remote: function() {},
            local: function(task, callback) {
                callback();
            },
            abort: function(msg) {
                throw new Error(msg);
            }
        };

        it('should abort if servers is present in config', function() {

            mockFactory.expects('create').returns({
                app: 'app',
                env: 'env',
                role: false,
                task: 'task',
                target: 'app',
                args: []
            });

            mockConfig.expects('getAppConfig').returns({
                servers: 'mock-servers'
            });

            assert.throws(function() { instances.create('task', plan, config); }, Error, 'Cannot dynamically determine servers, a static list is configured');
        });

        it('should abort if env is missing in context', function() {

            mockFactory.expects('create').returns({
                app: 'app',
                env: false,
                role: false,
                task: 'task',
                target: 'app',
                args: []
            });

            mockConfig.expects('getAppConfig').returns({});

            assert.throws(function() { instances.create('task', plan, config); }, Error, 'Missing env');
        });
    });
});
