var sinon = require('sinon');
var expect = require('chai').expect;
var assert = require('chai').assert;
var build = require('../../lib/tasks/build');
var factory = require('../../lib/context-factory');
var Config = require('../../lib/config');

describe('Build task', function() {

    var mockFactory, mockConfig;
    var config = new Config({});

    beforeEach(function() {
        mockFactory = sinon.mock(factory);
        mockConfig = sinon.mock(config);
    });

    afterEach(function() {
        mockFactory.restore();
        mockConfig.restore();
    });

    function setupEnvConfigExpectations(trueConfig) {

        var envConfig = [
            'branch',
            'defaultBranch',
            'awsConfig',
            'git',
            'build',
            'bucket'
        ];

        if (trueConfig) {
            mockConfig.expects('isEnvConfig').once().withArgs('app', trueConfig).returns(true);
        }

        for(var key in envConfig) {
            if (envConfig[key] != trueConfig) {
                mockConfig.expects('isEnvConfig').once().withArgs('app', envConfig[key]).returns(false);
            }
        }
    }

    describe('validation', function() {

        var plan = {
            remote: function() {},
            local: function(task, callback) {
                callback();
            },
            abort: function(msg) {
                throw new Error(msg);
            }
        };

        it('should abort if no env in context and branch is overriden at environment level', function() {

            setupEnvConfigExpectations('branch');

            mockFactory.expects('create').returns({
                app: 'app',
                env: false,
                role: false,
                task: 'task',
                target: 'app'
            });

            assert.throws(function() { build.create('task', plan, config); }, Error, 'Missing environment, the "branch" configuration is overriden on an environment level');
        });

        it('should abort if no env in context and defaultBranch is overriden at environment level', function() {

            setupEnvConfigExpectations('defaultBranch');

            mockFactory.expects('create').returns({
                app: 'app',
                env: false,
                role: false,
                task: 'task',
                target: 'app'
            });

            assert.throws(function() { build.create('task', plan, config); }, Error, 'Missing environment, the "defaultBranch" configuration is overriden on an environment level');
        });

        it('should abort if no env in context and awsConfig is overriden at environment level', function() {

            setupEnvConfigExpectations('awsConfig');

            mockFactory.expects('create').returns({
                app: 'app',
                env: false,
                role: false,
                task: 'task',
                target: 'app'
            });

            assert.throws(function() { build.create('task', plan, config); }, Error, 'Missing environment, the "awsConfig" configuration is overriden on an environment level');
        });

        it('should abort if no env in context and git is overriden at environment level', function() {

            setupEnvConfigExpectations('git');

            mockFactory.expects('create').returns({
                app: 'app',
                env: false,
                role: false,
                task: 'task',
                target: 'app'
            });

            assert.throws(function() { build.create('task', plan, config); }, Error, 'Missing environment, the "git" configuration is overriden on an environment level');
        });

        it('should abort if no env in context and build is overriden at environment level', function() {

            setupEnvConfigExpectations('build');

            mockFactory.expects('create').returns({
                app: 'app',
                env: false,
                role: false,
                task: 'task',
                target: 'app'
            });

            assert.throws(function() { build.create('task', plan, config); }, Error, 'Missing environment, the "build" configuration is overriden on an environment level');
        });

        it('should abort if context containes role', function() {

            mockFactory.expects('create').returns({
                app: 'app',
                env: 'env',
                role: 'role',
                task: 'task',
                target: 'app'
            });

            assert.throws(function() { build.create('task', plan, config); }, Error, 'task task can not be run on a role');
        });

        it('should abort if no git config is present', function() {

            mockConfig.expects('getAppConfig').withArgs('app', 'env').returns({});

            mockFactory.expects('create').returns({
                app: 'app',
                env: 'env',
                role: false,
                task: 'task',
                target: 'app'
            });

            assert.throws(function() { build.create('task', plan, config); }, Error, 'Missing configuration "git"');
        });
    });
});
