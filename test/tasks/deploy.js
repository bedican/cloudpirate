var sinon = require('sinon');
var expect = require('chai').expect;
var assert = require('chai').assert;
var deploy = require('../../lib/tasks/deploy');
var factory = require('../../lib/context-factory');
var Config = require('../../lib/config');

describe('Deploy task', function() {

    var mockFactory, mockConfig;
    var config = new Config({});

    beforeEach(function() {
        mockFactory = sinon.mock(factory);
        mockConfig = sinon.mock(config);
    });

    afterEach(function() {
        mockFactory.restore();
        mockConfig.restore();
    });

    describe('validation', function() {

        var plan = {
            remote: function() {},
            local: function(task, callback) {
                callback();
            },
            abort: function(msg) {
                throw new Error(msg);
            }
        };

        it('should abort if env is missing in context', function() {

            mockFactory.expects('create').returns({
                app: 'app',
                env: false,
                role: false,
                task: 'task',
                target: 'app',
                args: []
            });

            assert.throws(function() { deploy.create('task', plan, config); }, Error, 'task task must be run on an environment');
        });

        it('should abort if context containes role', function() {

            mockFactory.expects('create').returns({
                app: 'app',
                env: 'env',
                role: 'role',
                task: 'task',
                target: 'app.env.role',
                args: []
            });

            assert.throws(function() { deploy.create('task', plan, config); }, Error, 'task task must be run across all roles in an environment');
        });

        it('should abort if no path config is present', function() {

            mockConfig.expects('getAppConfig').withArgs('app', 'env').returns({});

            mockFactory.expects('create').returns({
                app: 'app',
                env: 'env',
                role: false,
                task: 'task',
                target: 'app.env',
                args: []
            });

            assert.throws(function() { deploy.create('task', plan, config); }, Error, 'Missing "path" configuration');
        });
    });
});
