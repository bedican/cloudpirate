var sinon = require('sinon');
var expect = require('chai').expect;
var assert = require('chai').assert;
var factory = require('../lib/context-factory');
var Config = require('../lib/config');

describe('Context Factory', function() {

    describe('#create', function() {
        it('should return context from app', function() {
            var plan = {
                runtime: {
                    task: 'task',
                    target: 'app'
                }
            };

            var config = new Config({});
            sinon.stub(config, 'isApp').returns(true);

            var context = factory.create(plan, config);

            expect(context.app).to.eql('app');
            expect(context.env).to.be.false;
            expect(context.role).to.be.false;
            expect(context.task).to.eql('task');
            expect(context.target).to.eql('app');
        });

        it('should return context from app and env', function() {
            var plan = {
                runtime: {
                    task: 'task',
                    target: 'app.env'
                }
            };

            var config = new Config({});
            sinon.stub(config, 'isApp').returns(true);

            var context = factory.create(plan, config);

            expect(context.app).to.eql('app');
            expect(context.env).to.eql('env');
            expect(context.role).to.be.false;
            expect(context.task).to.eql('task');
            expect(context.target).to.eql('app.env');
        });

        it('should return context with app, env and role', function() {
            var plan = {
                runtime: {
                    task: 'task',
                    target: 'app.env.role'
                }
            };

            var config = new Config({});
            sinon.stub(config, 'isApp').returns(true);

            var context = factory.create(plan, config);

            expect(context.app).to.eql('app');
            expect(context.env).to.eql('env');
            expect(context.role).to.eql('role');
            expect(context.task).to.eql('task');
            expect(context.target).to.eql('app.env.role');
        });

        it('should abort plan if unknown app', function() {
            var plan = {
                runtime: {
                    task: 'task',
                    target: 'not-an-app'
                },
                abort: function(message) {
                }
            };

            var mockPlan = sinon.mock(plan);
            mockPlan.expects('abort').throws(new Error('mocked'));

            var config = new Config({});
            sinon.stub(config, 'isApp').returns(false);

            assert.throws(function() { factory.create(plan, config); }, Error, 'mocked');
        });
    });
});
