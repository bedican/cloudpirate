var sinon = require('sinon');
var expect = require('chai').expect;
var Config = require('../lib/config');

describe('Config', function() {

    describe('#getApps', function() {

        var config = new Config({ apps: { app1: {}, app2: {} } });

        it('should return all apps defined within the configuration', function() {
            var apps = config.getApps();
            expect(apps).to.eql(['app1', 'app2']);
        });
    });

    describe('#isApp', function() {

        var config = new Config({ apps: { app1: {}, app2: {} } });

        it('should return true for defined apps', function() {
            expect(config.isApp('app1')).to.be.true;
            expect(config.isApp('app2')).to.be.true;
        });
        it('should return false for non defined apps', function() {
            expect(config.isApp('not-an-app')).to.be.false;
        });
    });

    describe('#isEnv', function() {

        var config = new Config({
            config: {
                envs: ['env1']
            },
            apps: {
                app1: {
                    default: {
                        envs: [ 'env2', 'env3' ]
                    },
                    env4: {
                    }
                },
                app2: {
                    default: {
                        envs: [ 'env5', 'env6' ]
                    }
                }
            }
        });

        it('should return true for envs in an app', function() {
            expect(config.isEnv('app1', 'env1')).to.be.true;
            expect(config.isEnv('app1', 'env2')).to.be.true;
            expect(config.isEnv('app1', 'env3')).to.be.true;
            expect(config.isEnv('app1', 'env4')).to.be.true;
        });

        it('should return false for non defined envs in an app', function() {
            expect(config.isEnv('app1', 'non-env')).to.be.false;
        });
    });

    describe('#isEnvConfig', function() {

        var config = new Config({ apps: { app1: { default: { 'key1': 'val1', 'key2': 'val2' }, 'env1': { 'key1': 'val1' } }, app2: {} } });

        it('should return true if environment configuration exists', function() {
            expect(config.isEnvConfig('app1', 'key1')).to.be.true;
        });

        it('should return false if environment configuration does not exist', function() {
            expect(config.isEnvConfig('app1', 'not-exist')).to.be.false;
            expect(config.isEnvConfig('app2', 'not-exist')).to.be.false;
            expect(config.isEnvConfig('app1', 'key2')).to.be.false;
        });
    });

    describe('#getEnvs', function() {

        var config = new Config({
            config: {
                envs: ['env1']
            },
            apps: {
                app1: {
                    default: {
                        envs: [ 'env2', 'env3' ]
                    },
                    env4: {
                    }
                },
                app2: {
                    default: {
                        envs: [ 'env5', 'env6' ]
                    }
                }
            }
        });

        it('should return all envs defined within an application', function() {
            var envs1 = config.getEnvs('app1');
            var envs2 = config.getEnvs('app2');

            expect(envs1).to.eql([ 'env1', 'env2', 'env3', 'env4' ]);
            expect(envs2).to.eql([ 'env1', 'env5', 'env6' ]);
        });
    });

    describe('#getAppConfig', function() {

        var config = new Config({
            config: {
                envs: ['env1'],
                key1: 'val1'
            },
            apps: {
                app1: {
                    default: {
                        envs: [ 'env2', 'env3' ],
                        key2: 'val2',
                        key3: 'val3'
                    },
                    env4: {
                        key4: 'val4'
                    }
                },
                app2: {
                    default: {
                        envs: [ 'env5', 'env6' ],
                        key5: 'val5'
                    }
                }
            }
        });

        it('should return merged application configuration', function() {
            expect(config.getAppConfig('app1', 'env1')).to.eql({
                envs: [ 'env2', 'env3' ],
                key1: 'val1',
                key2: 'val2',
                key3: 'val3'
            });
            expect(config.getAppConfig('app1', 'env4')).to.eql({
                envs: [ 'env2', 'env3' ],
                key1: 'val1',
                key2: 'val2',
                key3: 'val3',
                key4: 'val4'
            });
            expect(config.getAppConfig('app2', 'env1')).to.eql({
                envs: [ 'env5', 'env6' ],
                key1: 'val1',
                key5: 'val5'
            });
        });

        it('should return only main config block for unkown app or env', function() {
            expect(config.getAppConfig('not-an-app', 'non-env')).to.eql({
                envs: [ 'env1' ],
                key1: 'val1'
            });
        });

        it('should return only main config block for unkown app', function() {
            expect(config.getAppConfig('not-an-app', 'env1')).to.eql({
                envs: [ 'env1' ],
                key1: 'val1'
            });
        });

        it('should return merged app default config for unkown env', function() {
            expect(config.getAppConfig('app1', 'non-env')).to.eql({
                envs: [ 'env2', 'env3' ],
                key1: 'val1',
                key2: 'val2',
                key3: 'val3'
            });
        });

        it('should return merged app default config for no env', function() {
            expect(config.getAppConfig('app1', false)).to.eql({
                envs: [ 'env2', 'env3' ],
                key1: 'val1',
                key2: 'val2',
                key3: 'val3'
            });
        });
    });
});
