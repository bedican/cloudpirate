#! /usr/bin/env node

var nodeGetopt = require('node-getopt');
var plan = require('flightplan');
var CloudPirate = require('../lib/cloudpirate');

var help =
    "\nUsage:" +
    "\n  cloudpirate <task> <target>" +
    "\n\nOptions:" +
    "\n[[OPTIONS]]\n";

var opts = [
    ['f', 'file=ARG', 'CloudPirate configuration json file, defaults to cloudpirate.json within the current working directory.'],
    ['e', 'extend=ARG', 'CloudPirate extending js file, defaults to cloudpirate.js within the current working directory.'],
    ['h', 'help', 'Displays this help']
];

var getopt = nodeGetopt.create(opts).setHelp(help).bindHelp();
var opts = getopt.parseSystem();

function usage(message) {
    if (message) {
        console.info(message);
    }

    getopt.showHelp();
    process.exit();
}

var cloudpirate = new CloudPirate(plan);

try {
    cloudpirate.run(opts.argv, opts.options);
} catch(e) {
    usage(e.message);
}
