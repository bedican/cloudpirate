var fs = require('fs');
var tilde = require('expand-tilde');
var aws = require('./aws/aws');
var Config = require('./config');
var task = require('./tasks/task');

const JSFILE = 'cloudpirate.js';
const JSONFILE = 'cloudpirate.json';

var CloudPirate = function(plan) {
    this.plan = plan;
};

CloudPirate.prototype.getPlan = function() {
    return this.plan;
};

CloudPirate.prototype.configure = function(config) {
    var _this = this;

    var envs, roles, appConfig;
    var apps = config.getApps();
    var tasks = [];

    for(var a in apps) {

        this.plan.target(apps[a], []);

        envs = config.getEnvs(apps[a]);
        for(var e in envs) {
            appConfig = config.getAppConfig(apps[a], envs[e]);

            this.plan.target(apps[a] + '.' + envs[e], this.createTarget(envs[e], apps[a], false, appConfig));

            if (appConfig.roles) {
                roles = appConfig.roles;
                for(var r in roles) {
                    this.plan.target(apps[a] + '.' + envs[e] + '.' + r, this.createTarget(envs[e], apps[a], r, appConfig));

                    for(var t in roles[r]) {
                        if (tasks.indexOf(t) == -1) {
                            tasks.push(t);
                        }
                    }
                }
            }
        }
    }

    for(var t in tasks) {
        task.roletask.create(tasks[t], this.plan, config);
    }

    task.deploy.create('deploy', this.plan, config);
    task.build.create('build', this.plan, config);
    task.instances.create('servers', this.plan, config);
    task.version.create('version', this.plan, config);
};

CloudPirate.prototype.createTarget = function(env, app, role, config) {

    if ((!config.username) || (!config.sshKey)) {
        throw new Error('Missing username or sshKey configuration options.');
    }

    if (config.servers) {
        return this.createStaticTarget(config, role);
    }

    return this.createDynamicTarget(env, app, role, config);
};

CloudPirate.prototype.createStaticTarget = function(config, role) {

    var hosts = [];
    var servers;

    if (config.servers[role]) {
        servers = config.servers[role];
    } else if (role == 'single') {
        servers = [ config.servers.default[0] ];
    } else {
        servers = config.servers.default;
    }

    for (var key in servers) {
        hosts.push({
            host: servers[key],
            username: config.username,
            privateKey: tilde(config.sshKey)
        });
    }

    return hosts;
};

CloudPirate.prototype.createDynamicTarget = function(env, app, role, config) {
    return function(done) {
        aws.ec2.servers(env, app, role, config, function(err, servers) {
            if (err) return done(err);

            var hosts = [];
            for (var key in servers) {
                if (servers[key].host) {
                    hosts.push({
                        host: servers[key].host,
                        username: config.username,
                        privateKey: tilde(config.sshKey)
                    });
                }
            }

            if ((hosts.length) || (role != 'single')) {
                done(hosts);
                return;
            }

            aws.ec2.servers(env, app, false, config, function(err, servers) {
                if (err) return done(err);

                var hosts = [];
                for (var key in servers) {
                    if (servers[key].host) {
                        hosts.push({
                            host: servers[key].host,
                            username: config.username,
                            privateKey: tilde(config.sshKey)
                        });

                        break;
                    }
                }

                done(hosts);
            });
        });
    }
};

CloudPirate.prototype.extend = function(jsFile, config) {

    try {
        fs.statSync(jsFile);
    } catch(e) {
        return;
    }

    var extender = require(jsFile);

    if (typeof(extender) == 'function') {
        extender(this, config);
    }
};

CloudPirate.prototype.run = function(args, opts) {

    var filename = opts.file || (process.cwd() + '/' + JSONFILE);
    var config = new Config(JSON.parse(fs.readFileSync(filename, 'utf8')));

    var jsFile = opts.extend || (process.cwd() + '/' + JSFILE);

    this.configure(config);
    this.extend(jsFile, config);

    var task, target, action;

    if (args.length == 2) {
        task = args.shift();
        target = args.shift();
    } else {
        action = args.pop();
        if (!action) {
            throw new Error('Missing task or target');
        }

        action = action.split(':');

        task = action.shift();
        target = action.shift();
    }

    if ((!task) || (!target)) {
        throw new Error('Missing task or target');
    }

    this.plan.run(task, target);
};

module.exports = CloudPirate;
