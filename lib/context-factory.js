module.exports = {
    create: function(plan, config) {
        var task = plan.runtime.task;
        var target = plan.runtime.target;

        var appEnvRole = target.split('.');

        var app = appEnvRole.shift();
        var env = appEnvRole.shift() || false;
        var role = appEnvRole.shift() || false;

        if (!config.isApp(app)) {
            plan.abort('Unknown app');
        }

        return {
            app: app,
            env: env,
            role: role,
            task: task,
            target: target
        };
    }
};
