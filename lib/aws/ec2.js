var factory = require('./aws-service-factory');
var config = require('./config');

var aws = {

    tagKey: {
        env: 'cloudpirate.env',
        app: 'cloudpirate.app',
        role: 'cloudpirate.role'
    },

    servers: function(env, app, role, appConfig, callback) {

        var _this = this;

        var awsConfig = config.getConfig(appConfig);
        var ec2 = factory.ec2(awsConfig.aws);

        var params = {
            Filters: [
                {
                    Name: 'instance-state-name',
                    Values: [
                        'running'
                    ]
                },
                {
                    Name: 'tag-key',
                    Values: [
                        this.tagKey.env
                    ]
                },
                {
                    Name: 'tag-key',
                    Values: [
                        this.tagKey.app
                    ]
                }
            ]
        };

        if (role) {
            params.Filters.push({
                Name: 'tag-key',
                Values: [
                    this.tagKey.role
                ]
            });
        }

        ec2.describeInstances(params, function(err, data) {
            if (err) {
                callback(err);
                return;
            }

            var reservation, instance, tags;
            var servers = [];

            for (var x in data.Reservations) {
                reservation = data.Reservations[x];
                for (var y in reservation.Instances) {
                    instance = reservation.Instances[y];

                    tags = {};

                    for(var t in instance.Tags) {
                        tags[instance.Tags[t].Key] = instance.Tags[t].Value.split(',');
                    }

                    if ((!tags[_this.tagKey.env]) || (tags[_this.tagKey.env].indexOf(env) == -1)) {
                        continue;
                    }
                    if ((!tags[_this.tagKey.app]) || (tags[_this.tagKey.app].indexOf(app) == -1)) {
                        continue;
                    }
                    if (role) {
                        if ((!tags[_this.tagKey.role]) || (tags[_this.tagKey.role].indexOf(role) == -1)) {
                            continue;
                        }
                    }

                    servers.push({
                        id: instance.InstanceId,
                        host: instance.PublicDnsName || instance.PublicIpAddress,
                        ip: instance.PublicIpAddress,
                        ipPrivate: instance.PrivateIpAddress,
                        dns: instance.PublicDnsName,
                        dnsPrivate: instance.PrivateDnsName,
                        type: instance.InstanceType,
                        time: instance.LaunchTime,
                        tags : tags
                    });
                }
            }

            callback(false, servers);
        });
    }
};

module.exports = {
    servers: function(env, app, role, appConfig, callback) {
        aws.servers(env, app, role, appConfig, callback);
    }
};
