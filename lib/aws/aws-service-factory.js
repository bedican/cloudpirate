var AWS = require('aws-sdk');

module.exports = {
    ec2: function(config) {
        AWS.config.update(config);
        return new AWS.EC2();
    },
    s3: function(config) {
        AWS.config.update(config);
        return new AWS.S3({apiVersion: '2006-03-01'});
    }
};
