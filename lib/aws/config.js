var fs = require('fs');
var tilde = require('expand-tilde');

const JSONFILE = 'cloudpirate-aws.json';

module.exports = {
    getCwd: function() {
        return process.cwd();
    },
    getConfig: function(appConfig) {

        var config, filename;

        filename = appConfig.awsConfig ? tilde(appConfig.awsConfig) : false;
        filename = filename || (this.getCwd() + '/' + JSONFILE);

        try {
            config = JSON.parse(fs.readFileSync(filename, 'utf8'));
        } catch (e) {
            throw new Error('Failed to read aws configuration');
        }

        return config;
    }
};
