var fs = require('fs');
var factory = require('./aws-service-factory');
var config = require('./config');

var aws = {
    upload: function(filename, key, appConfig, callback) {

        var awsConfig = config.getConfig(appConfig);
        var body = fs.createReadStream(filename);

        var bucket = appConfig.bucket || awsConfig.s3.bucket;

        var params = {
            Bucket: bucket,
            Key: key,
            Body: body
        };

        var s3 = factory.s3(awsConfig.aws);

        s3.putObject(params)
            .on('success', function(response) {
                callback(true);
            })
            .on('error', function(response) {
                callback(false);
            })
            .send();
    },
    download: function(key, filename, appConfig, callback) {

        var awsConfig = config.getConfig(appConfig);
        var stream = fs.createWriteStream(filename);

        var bucket = appConfig.bucket || awsConfig.s3.bucket;

        var params = {
            Bucket: bucket,
            Key: key
        };

        var s3 = factory.s3(awsConfig.aws);

        s3.getObject(params)
            .on('success', function(response) {
                callback(true);
            })
            .on('error', function(response) {
                callback(false);
            })
            .createReadStream()
            .pipe(stream);
    },
    content: function(key, appConfig, callback) {

        var awsConfig = config.getConfig(appConfig);
        var bucket = appConfig.bucket || awsConfig.s3.bucket;

        var params = {
            Bucket: bucket,
            Key: key
        };

        var s3 = factory.s3(awsConfig.aws);

        s3.getObject(params)
            .on('success', function(response) {
                callback(response.data.Body.toString().trim());
            })
            .on('error', function(response) {
                callback(false);
            })
            .send();
    }
};

module.exports = aws;
