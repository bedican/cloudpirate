var Config = function(config) {
    this.config = config;
};

Config.prototype.getApps = function() {
    var apps = [];

    if (this.config.apps) {
        for(var key in this.config.apps) {
            apps.push(key);
        }
    }

    return apps;
};

Config.prototype.isEnv = function(app, env) {
    return (this.getEnvs(app).indexOf(env) != -1);
};

Config.prototype.isApp = function(app) {
    return this.config.apps[app] ? true : false;
};

Config.prototype.isEnvConfig = function(app, config) {

    if (!this.config.apps[app]) {
        return false;
    }

    for(var env in this.config.apps[app]) {
        if (env == 'default') {
            continue;
        }
        if (this.config.apps[app][env][config]) {
            return true;
        }
    }

    return false;
};

Config.prototype.getEnvs = function(app) {

    var envs = [];

    if ((this.config.config) && (this.config.config.envs)) {
        for(var key in this.config.config.envs) {
            envs.push(this.config.config.envs[key]);
        }
    }

    if (!this.config.apps[app]) {
        return envs;
    }

    if ((this.config.apps[app].default) && (this.config.apps[app].default.envs)) {
        var defaultEnvs = this.config.apps[app].default.envs;
        for(var key in defaultEnvs) {
            if (envs.indexOf(defaultEnvs[key]) == -1) {
                envs.push(defaultEnvs[key]);
            }
        }
    }

    for(var env in this.config.apps[app]) {
        if ((env != 'default') && (envs.indexOf(env) == -1)) {
            envs.push(env);
        }
    }

    return envs;
};

Config.prototype.getAppConfig = function(app, env) {

    var config = {};

    if (this.config.config) {
        for (var key in this.config.config) {
            config[key] = this.config.config[key];
        }
    }

    if (!this.config.apps[app]) {
        return config;
    }

    if (this.config.apps[app].default) {
        for(var key in this.config.apps[app].default) {
            config[key] = this.config.apps[app].default[key];
        }
    }

    if ((env) && (this.config.apps[app][env])) {
        for(var key in this.config.apps[app][env]) {
            config[key] = this.config.apps[app][env][key];
        }
    }

    return config;
};

module.exports = Config;
