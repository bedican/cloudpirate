var fs = require('fs');
var aws = require('../aws/aws');
var factory = require('../context-factory');

var deploy = {

    branch: false,
    timestamp: false,

    create: function(task, plan, config) {
        var _this = this;

        plan.local(task, function(transport) {
            var context = factory.create(plan, config);

            if (!context.env) {
                plan.abort(task + ' task must be run on an environment');
            }
            if (context.role) {
                plan.abort(task + ' task must be run across all roles in an environment');
            }

            var appConfig = config.getAppConfig(context.app, context.env);

            if (!appConfig.path) {
                plan.abort('Missing "path" configuration');
            }

            var branch = appConfig.defaultBranch ? appConfig.defaultBranch : 'master';

            _this.branch = appConfig.branch || transport.prompt('Branch? [' + branch + ']') || branch;
            _this.branch = _this.branch.replace(/\s/, '');

            _this.timestamp = (new Date()).getTime();
        });

        plan.remote(task, function(transport) {
            _this.setup(transport, plan, config, factory.create(plan, config));
        });

        plan.local(task, function(transport) {
            var context = factory.create(plan, config);

            _this.download(transport, plan, config, context);
            _this.upload(transport, plan, config, context);
        });

        plan.remote(task, function(transport) {
            var context = factory.create(plan, config);

            _this.unpack(transport, plan, config, context);
            _this.linkshared(transport, plan, config, context);
            _this.build(transport, plan, config, context);
            _this.symlink(transport, plan, config, context);
            _this.postsymlink(transport, plan, config, context);
            _this.cleanup(transport, plan, config, context);
        });

        plan.local(task, function(transport) {
            var context = factory.create(plan, config);

            _this.saveversion(transport, plan, config, context);
        });
    },
    setup: function(transport, plan, config, context) {
        var config = config.getAppConfig(context.app, context.env);

        var releases = config.path + '/releases';
        var shared = config.path + '/shared';

        transport.exec('[ -d ' + releases + ' ] || mkdir -p ' + releases);
        transport.exec('[ -d ' + shared + ' ] || mkdir -p ' + shared);
    },
    download: function(transport, plan, config, context) {
        var config = config.getAppConfig(context.app, context.env);

        var deployfrom = process.cwd() + '/deploy';

        try {
            fs.mkdirSync(deployfrom);
        } catch(e) {
        }

        var filename = context.app + '-' + this.branch + '.tar.gz';
        var download = deployfrom + '/' + filename;
        var key = context.app + '/' + filename;

        transport.waitFor(function(done) {
            aws.s3.download(key, download, config, function(success) {
                if (success) {
                    transport.log('File "' + filename + '" downloaded');
                } else {
                    plan.abort('Failed to download file "' + filename + '"');
                }

                done();
            });
        });
    },
    upload: function(transport, plan, config, context) {
        var config = config.getAppConfig(context.app, context.env);

        var deployfrom = process.cwd() + '/deploy';
        var filename = context.app + '-' + this.branch + '.tar.gz';
        var remote = config.path + '/releases';

        transport.with('cd ' + deployfrom, function() {
            transport.transfer([ filename ], remote);
            transport.rm(deployfrom + '/' + filename);
        });
    },
    unpack: function(transport, plan, config, context) {
        var config = config.getAppConfig(context.app, context.env);

        var releases = config.path + '/releases';
        var version = releases + '/' + this.timestamp;
        var filename = context.app + '-' + this.branch + '.tar.gz';

        transport.mkdir(version);

        transport.with('cd ' + releases, function() {
            transport.tar('-xzv -C ' + version + ' --strip-components=1 -f ' + filename);
            transport.rm(filename);
        });
    },
    linkshared: function(transport, plan, config, context) {
        var config = config.getAppConfig(context.app, context.env);

        var shared = config.path + '/shared';
        var releases = config.path + '/releases';
        var version = releases + '/' + this.timestamp;

        var item;

        transport.with('cd ' + version, function() {
            if (config.shared) {
                for(var s in config.shared) {

                    item = shared + '/' + config.shared[s];

                    transport.exec('([ -e ' + item + ' ] || [ ! -d ' + config.shared[s] + ' ] || mv ' + config.shared[s] + ' ' + item + ')');

                    transport.exec('([ ! -L ' + config.shared[s] + ' ] || rm ' + config.shared[s] + ')');
                    transport.exec('([ ! -f ' + config.shared[s] + ' ] || rm ' + config.shared[s] + ')');
                    transport.exec('([ ! -d ' + config.shared[s] + ' ] || rm -rf ' + config.shared[s] + ')');

                    transport.exec('([ -d ' + item + ' ] || mkdir -p ' + item + ')');

                    transport.ln('-s ' + item + ' ' + config.shared[s]);
                }
            }
        });
    },
    build: function(transport, plan, config, context) {
        var config = config.getAppConfig(context.app, context.env);

        if (!config.buildRemote) {
            return;
        }

        var releases = config.path + '/releases';
        var version = releases + '/' + this.timestamp;

        transport.with('cd ' + version, function() {
            transport.exec(config.buildRemote);
        });
    },
    symlink: function(transport, plan, config, context) {
        var config = config.getAppConfig(context.app, context.env);

        var releases = config.path + '/releases';
        var version = releases + '/' + this.timestamp;

        transport.with('cd ' + config.path, function() {
            transport.mv('current previous', {failsafe: true});
            transport.ln('-s ' + version + ' current');
        });
    },
    postsymlink: function(transport, plan, config, context) {
        var config = config.getAppConfig(context.app, context.env);

        if (!config.postSymlink) {
            return;
        }

        var releases = config.path + '/releases';
        var version = releases + '/' + this.timestamp;

        transport.with('cd ' + version, function() {
            transport.exec(config.postSymlink);
        });
    },
    cleanup: function(transport, plan, config, context) {
        var config = config.getAppConfig(context.app, context.env);

        if (!config.keep) {
            return;
        }

        var releases = config.path + '/releases';

        var list = transport.ls(releases, { silent: true }).stdout
            .trim()
            .split("\n")
            .slice(0, 0 - config.keep);

        transport.with('cd ' + releases, function() {
            for(var l in list) {
                transport.rm('-rf ' + list[l]);
            }
        });
    },
    saveversion: function(transport, plan, config, context) {
        var config = config.getAppConfig(context.app);

        var deployfrom = process.cwd() + '/deploy';
        var filename = context.app + '-' + this.branch + '.tar.gz';
        var version = 'current-version-' + context.env;

        var upload = deployfrom + '/' + context.app + '-' + version;
        var key = context.app + '/' + version;

        transport.echo('"' + filename + '" > ' + upload);

        transport.log('Uploading "' + version + '" to "' + key + '"');

        transport.waitFor(function(done) {
            aws.s3.upload(upload, key, config, function(success) {
                if (success) {
                    transport.log('"' + version + '" uploaded to "' + key + '"');
                } else {
                    transport.log('Failed to upload "' + version + '"');
                }

                done();
            });
        });

        transport.rm(upload);
    }
};

module.exports = {
    create: function(task, plan, config) {
        deploy.create(task, plan, config);
    }
};
