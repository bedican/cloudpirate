var fs = require('fs');
var aws = require('../aws/aws');
var factory = require('../context-factory');

var build = {

    branch: false,

    create: function(task, plan, config) {
        var _this = this;

        // We can only run this task on the local env (which has no servers)
        plan.local(task, function(transport) {
            var context = factory.create(plan, config);

            // If there is overriden environment config, there must be an env in the context.
            if (!context.env) {

                var envConfig = [
                    'branch',
                    'defaultBranch',
                    'awsConfig',
                    'git',
                    'build',
                    'bucket'
                ];

                for(var key in envConfig) {
                    if (config.isEnvConfig(context.app, envConfig[key])) {
                        plan.abort('Missing environment, the "' + envConfig[key] + '" configuration is overriden on an environment level');
                    }
                }
            }

            if (context.role) {
                plan.abort(task + ' task can not be run on a role');
            }

            var appConfig = config.getAppConfig(context.app, context.env);

            if (!appConfig.git) {
                plan.abort('Missing configuration "git"');
            }

            var branch = appConfig.defaultBranch ? appConfig.defaultBranch : 'master';

            _this.branch = appConfig.branch || transport.prompt('Branch? [' + branch + ']') || branch;
            _this.branch = _this.branch.replace(/\s/, '');
        });

        plan.local(task, function(transport) {
            var context = factory.create(plan, config);

            _this.clone(transport, plan, config, context);
            _this.build(transport, plan, config, context);
            _this.pack(transport, plan, config, context);
            _this.upload(transport, plan, config, context);
            _this.cleanup(transport, plan, config, context);
        });
    },
    clone: function(transport, plan, config, context) {
        var config = config.getAppConfig(context.app, context.env);

        var _this = this;

        var buildfrom = process.cwd() + '/build';
        var builddir = context.app + '-' + this.branch;
        var buildpath = buildfrom + '/' + builddir;
        var exists = false;

        try {
            fs.statSync(buildpath);
            exists = true;
        } catch(e) {
        }

        if (exists) {
            var remove = transport.prompt('Build directory "' + buildpath + '" exists, remove? [y]');
            if(remove !== 'y') {
                plan.abort('aborted');
            }

            transport.rm('-rf ' + buildpath);
        }

        transport.git('clone ' + config.git + ' ' + buildpath);

        transport.with('cd ' + buildpath, function() {
            transport.git('checkout '+ _this.branch);
        });
    },
    build: function(transport, plan, config, context) {
        var config = config.getAppConfig(context.app, context.env);

        if (!config.build) {
            return;
        }

        var buildfrom = process.cwd() + '/build';
        var builddir = context.app + '-' + this.branch;
        var buildpath = buildfrom + '/' + builddir;

        transport.with('cd ' + buildpath, function() {
            transport.exec(config.build);
        });
    },
    pack: function(transport, plan, config, context) {
        var config = config.getAppConfig(context.app, context.env);

        var buildfrom = process.cwd() + '/build';
        var builddir = context.app + '-' + this.branch;
        var filename = context.app + '-' + this.branch + '.tar.gz';

        transport.with('cd ' + buildfrom, function() {
            transport.tar('--exclude=".git" -cvzf ' + filename + ' ' + builddir);
        });
    },
    upload: function(transport, plan, config, context) {
        var config = config.getAppConfig(context.app, context.env);

        var buildfrom = process.cwd() + '/build';
        var filename = context.app + '-' + this.branch + '.tar.gz';

        var upload = buildfrom + '/' + filename;
        var key = context.app + '/' + filename;

        transport.log('Uploading "' + filename + '" to "' + key + '"');

        transport.waitFor(function(done) {
            aws.s3.upload(upload, key, config, function(success) {
                if (success) {
                    transport.log('File "' + filename + '" uploaded to "' + key + '"');
                } else {
                    transport.log('Failed to upload file "' + filename + '"');
                }

                done();
            });
        });
    },
    cleanup: function(transport, plan, config, context) {
        var config = config.getAppConfig(context.app, context.env);

        var buildfrom = process.cwd() + '/build';
        var builddir = context.app + '-' + this.branch;
        var filename = context.app + '-' + this.branch + '.tar.gz';
        var upload = buildfrom + '/' + filename;
        var buildpath = buildfrom + '/' + builddir;

        transport.rm(upload);
        transport.rm('-rf ' + buildpath);
    }
};

module.exports = {
    create: function(task, plan, config) {
        build.create(task, plan, config);
    }
};
