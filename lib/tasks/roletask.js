var factory = require('../context-factory');

var roletask = {
    create: function(task, plan, config) {
        var _this = this;

        // Ensure we are targeting a role with this task
        plan.local(task, function(transport) {
            var context = factory.create(plan, config);
            var appConfig = config.getAppConfig(context.app, context.env);

            if (!context.role) {
                plan.abort(task + ' can not be run, no role in context');
            }
            if (!appConfig.roles) {
                plan.abort(task + ' can not be run, no roles defined for app');
            }
            if (!appConfig.roles[context.role]) {
                plan.abort(task + ' can not be run, unknown role ' + context.role);
            }
            if (!appConfig.roles[context.role][task]) {
                plan.abort(task + ' task does not exist for role ' + context.role);
            }
        });

        plan.remote(task, function(transport) {
            var context = factory.create(plan, config);
            var appConfig = config.getAppConfig(context.app, context.env);

            var current = appConfig.path + '/current';
            var command = appConfig.roles[context.role][task];

            transport.with('cd ' + current, function() {
                transport.exec(command);
            });
        });
    }
};

module.exports = {
    create: function(task, plan, config) {
        roletask.create(task, plan, config);
    }
};
