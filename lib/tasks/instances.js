var Table = require('cli-table');
var aws = require('../aws/aws');
var factory = require('../context-factory');

var instances = {
    create: function(task, plan, config) {
        var _this = this;

        plan.local(task, function(transport) {
            var context = factory.create(plan, config);
            var appConfig = config.getAppConfig(context.app, context.env);

            if (!context.env) {
                plan.abort('Missing env');
            }
            if (appConfig.servers) {
                plan.abort('Cannot dynamically determine servers, a static list is configured');
            }
        });

        plan.local(task, function(transport) {
            _this.list(transport, plan, config, factory.create(plan, config));
        });
    },
    list: function(transport, plan, config, context) {

        var config = config.getAppConfig(context.app, context.env);

        var servers = transport.waitFor(function(done) {
            aws.ec2.servers(context.env, context.app, context.role, config, function(err, servers) {
                done(err || servers);
            });
        });

        var consoleWidth = process.stdout.getWindowSize()[0];

        var table = new Table({
            head: ['ID'.bold.cyan, 'Type'.bold.cyan, 'Up Since'.bold.cyan, 'Host'.bold.cyan],
            colWidths: [
                parseInt((consoleWidth / 100) * 10),
                parseInt((consoleWidth / 100) * 10),
                parseInt((consoleWidth / 100) * 30),
                parseInt((consoleWidth / 100) * 45)
            ],
            truncate: false
        });

        for(var key in servers) {
            table.push(
                [servers[key].id, servers[key].type, servers[key].time, servers[key].host]
            );
        }

        console.log(table.toString());
    }
};

module.exports = {
    create: function(task, plan, config) {
        instances.create(task, plan, config);
    }
};
