var Table = require('cli-table');
var aws = require('../aws/aws');
var factory = require('../context-factory');

var version = {
    create: function(task, plan, config) {
        var _this = this;

        plan.local(task, function(transport) {
            var context = factory.create(plan, config);

            if (context.role) {
                plan.abort(task + ' task must be run across all roles in an environment');
            }
        });

        plan.local(task, function(transport) {
            _this.version(transport, plan, config, factory.create(plan, config));
        });
    },
    version: function(transport, plan, config, context) {

        var envs = (context.env) ? [ context.env ] : config.getEnvs(context.app);

        var appConfig, bucketKey, version, versions = {};

        for (var key in envs) {

            appConfig = config.getAppConfig(context.app, envs[key]);
            bucketKey = context.app + '/current-version-' + envs[key];

            version = transport.waitFor(function(done) {
                aws.s3.content(bucketKey, appConfig, function(data) {
                    done(data);
                });
            });

            if (!version) {
                transport.log('Failed to obtain version for ' + envs[key] + ' environment');
                continue;
            }

            versions[envs[key]] = version;
        }

        var consoleWidth = process.stdout.getWindowSize()[0];

        var table = new Table({
            head: ['Environment'.bold.cyan, 'Package'.bold.cyan],
            colWidths: [
                parseInt((consoleWidth / 100) * 40),
                parseInt((consoleWidth / 100) * 55)
            ],
            truncate: false
        });

        for(var env in versions) {
            table.push([env, versions[env]]);
        }

        console.log(table.toString());
    }
};

module.exports = {
    create: function(task, plan, config) {
        version.create(task, plan, config);
    }
};
