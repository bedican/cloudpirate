module.exports = {
    build: require('./build'),
    deploy: require('./deploy'),
    instances: require('./instances'),
    roletask: require('./roletask'),
    version: require('./version')
};
